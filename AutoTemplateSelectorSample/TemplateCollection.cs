﻿namespace AutoTemplateSelectorSample
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows;

    public class TemplateCollection : Collection<DataTemplate>
    {
        public DataTemplate Find(Type type)
        {
            foreach (var candidate in this.Items)
            {
                if (Equals(candidate.DataType, type))
                {
                    return candidate;
                }
            }

            return null;
        }

        protected override void InsertItem(int index, DataTemplate item)
        {
            this.ValidateItem(item);
            base.InsertItem(index, item);
        }

        protected override void SetItem(int index, DataTemplate item)
        {
            this.ValidateItem(item);
            base.SetItem(index, item);
        }

        private void ValidateItem(DataTemplate item)
        {
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            if (item.DataType == null)
            {
                throw new InvalidOperationException("DataTemplate.DataType cannot be null.");
            }

            if (this.Find((Type)item.DataType) != null)
            {
                throw new InvalidOperationException("Template already added for type.");
            }
        }
    }
}
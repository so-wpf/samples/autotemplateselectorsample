﻿namespace AutoTemplateSelectorSample
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Markup;

    [ContentProperty("Templates")]
    public class AutoDataTemplateSelector : DataTemplateSelector
    {
        private TemplateCollection templates;

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (item == null)
            {
                return null;
            }

            return (this.templates.Find(item.GetType()) ??
                    (container as FrameworkElement)?.FindResource(new DataTemplateKey(item.GetType()))) as DataTemplate;
        }

        public TemplateCollection Templates => this.templates ?? (this.templates = new TemplateCollection());
    }
}